% Copyright (c) 2015 Melvin Eloy Irizarry-Gelpí
\chapter{Kinematic Invariants}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
This appendix contains an overview of different kinematic invariants.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Preliminaries}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The signature of the spacetime metric is ``mostly minus''. This means that given two spacetime vectors $V$ and $W$ with temporal components $V^{0}$ and $W^{0}$ and spatial components $\mathbf{V}$ and $\mathbf{W}$, the inner product of $V$ with $W$ is given by
\begin{equation}
	V \cdot W = V^{0}W^{0} - \mathbf{V} \cdot \mathbf{W}.
\end{equation}
I will work in a spacetime with one temporal dimension and $D-1$ spatial dimensions.

Every external quantum carries an energy-momentum vector $p$ with temporal component $E$ (the energy) and spatial component $\mathbf{p}$ (the spatial momentum). A quantum can be either \textbf{slow} ($p^{2} > 0$), \textbf{null} ($p^{2} = 0$), or \textbf{fast} ($p^{2} < 0$). I will work with external quanta that is either slow or null. Thus, every external quantum has a mass $m \geq 0$. Relativistically, the mass, energy, and spatial momentum of a (slow or null) quantum are related via
\begin{equation}
	p^{2} = m^{2} \quad \Longrightarrow \quad E^{2} = m^{2} + \mathbf{p}^{2}.
\end{equation}
This is known as an \textbf{on-shell constraint}. I will assume that each external quantum is on-shell.

Instead of the usual simplification of having the external quanta to be either all incoming or all outgoing, I will insist on spliting the external quanta into an incoming set and an outgoing set. This will allow me to treat all energies as being positive.

Two distinct energy-momentum vectors are \textbf{adjacent} if the associated quanta are either both incoming or both outgoing. Otherwise, the two energy-momentum vectors are \textbf{nonadjacent}.

A general scattering process with $M$ incoming quanta and $N$ outgoing quanta has the form
\begin{equation}
	\phi_{1}(p_{1}) + \ldots + \phi_{M}(p_{M}) \longrightarrow \psi_{1}(q_{1}) + \ldots + \psi_{N}(q_{N}).
\end{equation}
This corresponds to an $(M+N)$-point process. Here each of the incoming/outgoing quanta is denoted with a different symbol. A pair of quanta with different symbols are understood to be of different type (e.g. a quantum of type $\phi_{1}$ and a quantum of type $\phi_{2}$ have different masses and quantum numbers). The sum of all incoming energy-momentum vectors must equal the sum of all outgoing energy-momentum vectors:
\begin{equation}
	p_{1} + \ldots p_{M} = q_{1} + \ldots q_{N}.
\end{equation}
This is known as a \textbf{conservation constraint}. All scattering processes that I will consider have a conservation constraint.

A scattering process is \textbf{elastic} if for every incoming quantum of a particular type there is a corresponding outgoing quantum of the same type. That is, a generic elastic scattering process with $N$ incoming/outgoing quanta has the form
\begin{equation}
	\phi_{1}(p_{1}) + \ldots + \phi_{N}(p_{N}) \longrightarrow \phi_{1}(q_{1}) + \ldots + \phi_{N}(q_{N}).
\end{equation}
Every elastic scattering process has an even number of external quanta. In a $2N$-point elastic process you can have at most $N$ distinct types of external quanta. Such an elastic process has \textbf{elastic diversity index} $N$ and is \textbf{maximally diverse}. On the other end, in a \textbf{minimally diverse} elastic process all of the external quanta is of the same type. Minimal diversity leads to \textbf{permutation symmetries}.

Besides the $2N$ on-shell constraints and the conservation constraint, the $2N$ energy-momentum vectors in a $2N$-point elastic process also satisfy $N$ constraints of the form
\begin{equation}
	p_{1}^{2} = q_{1}^{2}, \quad p_{2}^{2} = q_{2}^{2}, \quad \ldots \quad p_{N}^{2} = q_{N}^{2}.
\end{equation}
These are known as \textbf{elasticity constraints}. Effectively, these constraints restric the number of independent masses to be at most $N$.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Energy-Momentum Vectors}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
I will work in $D$ spacetime dimensions. This means that every energy-momentum vector has an energy component and $D-1$ spatial momentum components. In a scattering process with $M$ incoming quanta and $N$ outgoing quanta there are a total of $(M + N)$ energy variables and $(M + N) (D - 1)$ spatial momentum components. The scattering amplitude is a function of the energy-momentum vectors, so it is really a function of $(M + N)D$ variables. However, if there is a symmetry invariance, then the number of independent variables is much smaller.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Translation Invariance}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
In $D$ spacetime dimensions there are $D$ independent translations. Thus, a translation-invariant scattering amplitude for a process with $M$ incoming and $N$ outgoing off-shell quanta is a function of
\begin{equation}
	\operatorname{Tran}(M, N, D) = (M + N - 1)D
\end{equation}
variables. This agrees with the expectation from the conservation of momentum: the conservation constraint allows you to write one of the energy-momentum vectors in terms of the other $M + N - 1$ vectors.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Lorentz Invariance}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
In $D$ spacetime dimensions there are $D(D - 1) / 2$ independent Lorentz transformations. Thus, a Lorentz-invariant scattering amplitude for a process with $M$ incoming and $N$ outgoing off-shell quanta is a function of
\begin{equation}
	\operatorname{Lore}(M, N, D) = (M + N)D - \frac{D (D - 1)}{2}
\end{equation}
variables.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Poincar\'{e} Invariance}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Poincar\'{e} transformations include both translations and Lorentz transformations. Thus, a Poincar\'{e}-invariant scattering amplitude for a process with $M$ incoming and $N$ outgoing off-shell quanta is a function of
\begin{equation}
	\operatorname{Poin}(M, N, D) = (M + N) D - \frac{D (D + 1)}{2}
\end{equation}
variables. Setting $n = M + N$ and $D = 4$ leads to
\begin{equation}
	\operatorname{Poin}(n, 4) = 4n - 10.
\end{equation}
Usually the $n$ external energy-momentum vectors satisfy an on-shell constraint, so you really have $\operatorname{Poin}(n, 4) - n = 3n - 10$ independent variables which is positive when $n > 3$.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Conformal Invariance}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Conformal transformations include one dilatation, $D$ conformal boosts, and the Poincar\'{e} transformations. Thus, a conformal-invariant scattering amplitude for a process with $M$ incoming and $N$ outgoing off-shell quanta is a function of
\begin{equation}
	\operatorname{Conf}(M, N, D) = (M + N)D - \frac{(D + 2) (D + 1)}{2}
\end{equation}
variables. Setting $n = M + N$ and $D = 4$ leads to
\begin{equation}
	\operatorname{Conf}(n, 4) = 4n - 15.
\end{equation}
Usually the $n$ external energy-momentum vectors satisfy an on-shell constraint, so you really have $\operatorname{Conf}(n, 4) - n = 3(n - 5)$ independent variables which is positive when $n > 5$.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Thresholds and Pseudothresholds}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
When working with external massive quanta, you find that particular linear combinations of the masses appear often. Given $n + 1$ distinct masses, you can construct a \textbf{threshold value} and a set of $2^{n} - 1$ \textbf{pseudothreshold values}. The threshold value is always given by the square of the sum of the masses, while the pseudothreshold values are found by taking the square of a combination of sums and differences of masses. Just like for Mandelstam invariants below, I will refer to these values as the $(n + 1)$-threshold and $(n + 1)$-pseudothreshold values. Taking the average over the $(n + 1)$-threshold value and the $2^{n} - 1$ $(n + 1)$-pseudothreshold values gives the sum of squares of the $n + 1$ masses.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Two Quanta}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Given two distinct masses $\red{m}$, and $\blue{m}$, the \textbf{2-threshold value} is
\begin{equation}
	(\red{m} + \blue{m})^{2},
\end{equation}
and the \textbf{2-pseudothreshold value} is
\begin{equation}
	(\red{m} - \blue{m})^{2}.
\end{equation}
In the limit $\blue{m} \rightarrow 0$ the 2-threshold and 2-pseudothreshold become the same.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Three Quanta}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Given three distinct masses $\red{m}$, $\blue{m}$, and $\green{m}$, the \textbf{3-threshold value} is
\begin{equation}
	(\red{m} + \blue{m} + \green{m})^{2},
\end{equation}
and the three \textbf{3-pseudothreshold values} are
\begin{equation}
	(\red{m} - \blue{m} - \green{m})^{2}, \quad (\red{m} - \blue{m} + \green{m})^{2}, \quad (\red{m} + \blue{m} - \green{m})^{2}.
\end{equation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Four Quanta}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Given four distinct masses $\cyan{m}$, $\magenta{m}$, $\yellow{m}$, and $\gray{m}$ the \textbf{4-threshold value} is
\begin{equation}
	(\cyan{m} + \magenta{m} + \yellow{m} + \gray{m})^{2},
\end{equation}
and the seven \textbf{4-pseudothreshold values} are
\begin{equation}
\begin{split}
	(\cyan{m} + \magenta{m} - \yellow{m} - \gray{m})^{2}, \quad (\cyan{m} - \magenta{m} + \yellow{m} - \gray{m})^{2}, \quad (\cyan{m} - \magenta{m} - \yellow{m} + \gray{m})^{2}, \\
	(\cyan{m} - \magenta{m} - \yellow{m} - \gray{m})^{2}, \quad (\cyan{m} - \magenta{m} + \yellow{m} + \gray{m})^{2}, \\
	(\cyan{m} + \magenta{m} - \yellow{m} + \gray{m})^{2}, \quad (\cyan{m} + \magenta{m} + \yellow{m} - \gray{m})^{2}.
\end{split}
\end{equation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Mandelstam Invariants}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The Mandelstam momentum invariants are Lorentz scalar quantities that are made with linear combinations of energy-momentum vectors from different external quanta. The particular linear combination depends on whether the energy-momentum vectors are adjacent or nonadjacent.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Two Quanta}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Given two energy-momentum vectors $p_{I}$ and $p_{J}$, the \textbf{adjacent 2-Mandelstam} invariant is given by
\begin{equation}
	s_{IJ} \equiv (p_{I} + p_{J})^{2},
\end{equation}
and the \textbf{nonadjacent 2-Mandelstam} invariant is given by
\begin{equation}
	t_{IJ} \equiv (p_{I} - p_{J})^{2}.
\end{equation}
Note that
\begin{equation}
	s_{IJ} - m_{I}^{2} - m_{J}^{2} = m_{I}^{2} + m_{J}^{2} - t_{IJ},
\end{equation}
so in practice you either work with $s_{IJ}$ or $t_{IJ}$. Since
\begin{equation}
	t_{IJ} = 2m_{I}^{2} + 2m_{J}^{2} - s_{IJ},
\end{equation}
it follows that
\begin{align}
	s_{IJ} < (m_{I} - m_{J})^{2} \quad &\Longrightarrow \quad t_{IJ} > (m_{I} + m_{J})^{2}, \\
	s_{IJ} = (m_{I} - m_{J})^{2} \quad &\Longrightarrow \quad t_{IJ} = (m_{I} + m_{J})^{2}, \\
	(m_{I} - m_{J})^{2} < s_{IJ} < m_{I}^{2} + m_{J}^{2} \quad &\Longrightarrow \quad m_{I}^{2} + m_{J}^{2} < t_{IJ} < (m_{I} + m_{J})^{2}, \\
	s_{IJ} = m_{I}^{2} + m_{J}^{2} \quad &\Longrightarrow \quad t_{IJ} = m_{I}^{2} + m_{J}^{2}, \\
	m_{I}^{2} + m_{J}^{2} < s_{IJ} < (m_{I} + m_{J})^{2} \quad &\Longrightarrow \quad (m_{I} - m_{J})^{2} < t_{IJ} < m_{I}^{2} + m_{J}^{2}, \\
	s_{IJ} = (m_{I} + m_{J})^{2} \quad &\Longrightarrow \quad t_{IJ} = (m_{I} - m_{J})^{2}, \\
	s_{IJ} > (m_{I} + m_{J})^{2} \quad &\Longrightarrow \quad t_{IJ} < (m_{I} - m_{J})^{2}.
\end{align}

With $N$ energy-momentum vectors you can make a total of
\begin{equation}
	{N \choose 2} = \frac{N(N-1)}{2}
\end{equation}
possible 2-Mandelstam invariants. However, if there is a conservation constraint, then there are $N$ linear relations that reduce the number of independent invariants to be
\begin{equation}
	\mathfrak{M}_{2}(N) = \frac{N(N-1)}{2} - N = \frac{N(N-3)}{2}, \quad N > 4.
\end{equation}
Some particular values of $\mathfrak{M}_{2}(N)$ listed in Table \ref{tablM2}.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{table}
\centering
\begin{tabular}{|c|cccccc|}
\hline
$N$ & 3 & 4 & 5 & 6 & 7 & 8 \\ \hline
$\mathfrak{M}_{2}(N)$ & - & 2 & 5 & 9 & 14 & 20 \\ 
$\mathfrak{M}_{3}(N)$ & - & - & - & 10 & 35 & 56 \\
$\mathfrak{M}_{4}(N)$ & - & - & - & - & - & 35 \\ \hline
\end{tabular}
\caption{Values of $\mathfrak{M}_{n}(N)$ for $n = 2,3,4$.}
\label{tablM2}
\end{table}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Three Quanta}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Now consider three energy-momentum vectors $p_{I}$, $p_{J}$, and $p_{K}$. The \textbf{adjacent 3-Mandelstam} invariant is given by
\begin{equation}
	s_{IJK} \equiv (p_{I} + p_{J} + p_{K})^{2}.
\end{equation}
Now there are three types of \textbf{nonadjacent 3-Mandelstam} invariants:
\begin{align}
	a_{IJK} &\equiv (p_{I} - p_{J} - p_{K})^{2}, \\
	b_{IJK} &\equiv (p_{I} - p_{J} + p_{K})^{2}, \\
	c_{IJK} &\equiv (p_{I} + p_{J} - p_{K})^{2}.
\end{align}
Any 3-Mandelstam invariant can always be written in terms of 2-Mandelstam invariants and masses:
\begin{align}
	s_{IJK} &= s_{IJ} + s_{IK} + s_{JK} - m_{I}^{2} - m_{J}^{2} - m_{K}^{2} ,\\
	a_{IJK} &= t_{IJ} + t_{IK} + s_{JK} - m_{I}^{2} - m_{J}^{2} - m_{K}^{2} ,\\
	b_{IJK} &= t_{IJ} + s_{IK} + t_{JK} - m_{I}^{2} - m_{J}^{2} - m_{K}^{2} ,\\
	c_{IJK} &= s_{IJ} + t_{IK} + t_{JK} - m_{I}^{2} - m_{J}^{2} - m_{K}^{2} .
\end{align}
Note that
\begin{equation}
	s_{IJK} + a_{IJK} + b_{IJK} + c_{IJK} = 4m_{I}^{2} + 4m_{J}^{2} + 4m_{K}^{2}.
\end{equation}
With $N$ energy-momentum vectors, you can make a total of
\begin{equation}
	\mathfrak{M}_{3}(N) = {N \choose 3} = \frac{N(N-1)(N-2)}{6}, \quad N > 6,
\end{equation}
possible 3-Mandelstam invariants. If there is a conservation constraint, then $\mathfrak{M}_{3}(6) = 10$.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Four Quanta}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Next, consider four energy-momentum vectors $p_{I}$, $p_{J}$, $p_{K}$, and $p_{L}$. Now there are six adjacency pairs. This leads to three adjacency possibilities. If all six pairs are adjacent, then the \textbf{4-Mandelstam} invariant is given by
\begin{equation}
	s_{IJKL} \equiv (p_{I} + p_{J} + p_{K} + p_{L})^{2}.
\end{equation}
Another possibility is that only three pairs are adjacent. Then the \textbf{4-Mandelstam} invariant is given by one of four possibilities:
\begin{align}
	t_{IJKL}{}^{(1)} &\equiv (p_{I} - p_{J} - p_{K} - p_{L})^{2}, \\
	t_{IJKL}{}^{(2)} &\equiv (p_{I} - p_{J} + p_{K} + p_{L})^{2}, \\
	t_{IJKL}{}^{(3)} &\equiv (p_{I} + p_{J} - p_{K} + p_{L})^{2}, \\
	t_{IJKL}{}^{(4)} &\equiv (p_{I} + p_{J} + p_{K} - p_{L})^{2}.
\end{align}
The last possibility is that only two pairs are adjacent. Then the \textbf{4-Mandelstam} invariant is given by one of three possibilities:
\begin{align}
	u_{IJKL}{}^{(1)} &\equiv (p_{I} + p_{J} - p_{K} - p_{L})^{2}, \\
	u_{IJKL}{}^{(2)} &\equiv (p_{I} - p_{J} + p_{K} - p_{L})^{2}, \\
	u_{IJKL}{}^{(3)} &\equiv (p_{I} - p_{J} - p_{K} + p_{L})^{2}.	
\end{align}
Just like a 3-Mandelstam invariant, a 4-Mandelstam invariant can always be written in terms of 2-Mandelstam invariants and masses. For example,
\begin{equation}
	s_{IJKL} = s_{IJ} + s_{IK} + s_{IL} + s_{JK} + s_{JL} + s_{KL} - 2m_{I}^{2} - 2m_{J}^{2} - 2m_{K}^{2} - 2m_{L}^{2}.
\end{equation}
With $N$ energy-momentum vectors, you can make a total of
\begin{equation}
	\mathfrak{M}_{4}(N) = {N \choose 4} = \frac{N(N-1)(N-2)(N-3)}{24}, \quad N > 8,
\end{equation}
possible 4-Mandelstam invariants. If there is a conservation constraint, then $\mathfrak{M}_{4}(8) = 35$.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Regge Invariants}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
At this point it is not clear why you would bother working with $n$-Mandelstam invariants for $n > 2$ since these invariants can always be written in terms of 2-Mandelstam invariants and masses. In order to define a 2-Mandelstam invariant you need two distinct energy-momentum vectors. If both of these describe slow quanta, then one can also define what I call a Regge invariant. If the pair of quanta is adjacent, then the \textbf{Regge invariant} is given by
\begin{equation}
	\sigma_{IJ} \equiv \frac{p_{I} \cdot p_{J}}{\sqrt{p_{I}^{2}} \sqrt{p_{J}^{2}}} = \frac{s_{IJ} - m_{I}^{2} - m_{J}^{2}}{2m_{I} m_{J}}.
\end{equation}
Otherwise the pair of quanta is nonadjacent and the \textbf{Regge invariant} is given by
\begin{equation}
	\tau_{IJ} \equiv -\frac{p_{I} \cdot p_{J}}{\sqrt{p_{I}^{2}} \sqrt{p_{J}^{2}}} = \frac{t_{IJ} - m_{I}^{2} - m_{J}^{2}}{2m_{I} m_{J}}.
\end{equation}
In contrast with the 2-Mandelstam invariants, the Regge invariants are dimensionless. Indeed, they are functions of two dimensionless ratios:
\begin{equation}
	\sigma_{IJ}\left( \frac{s_{IJ}}{m_{I} m_{J}}, \frac{m_{I}}{m_{J}} \right), \quad \tau_{IJ}\left( \frac{t_{IJ}}{m_{I} m_{J}}, \frac{m_{I}}{m_{J}} \right).
\end{equation}
Note that
\begin{align}
	s_{IJ} < (m_{I} - m_{J})^{2} \quad &\Longrightarrow \quad \sigma_{IJ} < -1, \\
	s_{IJ} = (m_{I} - m_{J})^{2} \quad &\Longrightarrow \quad \sigma_{IJ} = -1, \\
	(m_{I} - m_{J})^{2} < s_{IJ} < m_{I}^{2} + m_{J}^{2} \quad &\Longrightarrow \quad -1 < \sigma_{IJ} < 0, \\
	s_{IJ} = m_{I}^{2} + m_{J}^{2} \quad &\Longrightarrow \quad \sigma_{IJ} = 0, \\
	m_{I}^{2} + m_{J}^{2} < s_{IJ} < (m_{I} + m_{J})^{2} \quad &\Longrightarrow \quad 0 < \sigma_{IJ} < 1, \\
	s_{IJ} = (m_{I} + m_{J})^{2} \quad &\Longrightarrow \quad \sigma_{IJ} = 1, \\
	s_{IJ} > (m_{I} + m_{J})^{2} \quad &\Longrightarrow \quad \sigma_{IJ} > 1.
\end{align}
Each of these regions describes a different kinematic regime. Regge invariants appear in Regge trajectory functions.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Dual Spacetime}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
...
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Simplicial Invariants}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
...
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{1-Simplex Invariants}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
...
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{2-Simplex Invariants}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
...
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{3-Simplex Invariants}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
...
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{4-Simplex Invariants}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
...
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Dual Conformal Invariants}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The dual coordinate intervals $d_{I} - d_{J}$ allows you to build \textbf{dual Poincar\'{e} invariants}:
\begin{equation}
	(d_{I} - d_{J})^{2}.
\end{equation}
The set of dual Poincar\'{e} invariants is the set of polygonal Mandelstam invariants and the masses. Given the dual coordinates of four distinct points in the dual spacetime, one can construct a \textbf{dual conformal ratio}:
\begin{equation}
	[I, J; K, L] \equiv \frac{(d_{I} - d_{K})^{2} (d_{J} - d_{L})^{2}}{(d_{I} - d_{L})^{2} (d_{J} - d_{K})^{2}}.
\end{equation}
The dual conformal ratio is invariant under dual conformal transformations.

There are $4! = 24$ possible permutations of the four coordinates, so you could expect 24 different values for the dual conformal ratio. However, swapping the coordinates pair-wise leaves $[I, J; K, L]$ invariant:
\begin{equation}
	[I, J; K, L] = [J, I; L, K] = [K, L; I, J] = [L, K; J, I].
\end{equation}
This means that out of the 24 possible values, only six are different:
\begin{equation}
\begin{split}
	[I, J; K, L] = u, \quad [I, K; J, L] &= v, \quad [I, L; K, J] = \frac{u}{v}, \\
	[I, J; L, K] = \frac{1}{u}, \quad [I, K; L, J] &= \frac{1}{v}, \quad [I, L; J, K] = \frac{v}{u}.
\end{split}
\end{equation}
If $u$ and $v$ are given, then all six different values are known. Thus, for a given quartet of coordinates only two values are independent. Note that each of the six different values can be written in the form:
\begin{equation}
	\frac{a_{u} u + a_{v} v + b}{c_{u} u + c_{v} v + d},
\end{equation}
which looks like a generalized M\"{o}bius transformation.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Speed and Rapidity}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
A slow relativistic quantum with mass $m$, spatial momentum vector $\mathbf{p}$ and energy $E$ has a \textbf{speed} $\vert \mathbf{v} \vert$ given by
\begin{equation}
	\vert \mathbf{v} \vert = \frac{\sqrt{E^{2} - m^{2}}}{E} = \frac{\vert \mathbf{p} \vert}{\sqrt{m^{2} + \mathbf{p}^{2}}} = \frac{\vert \mathbf{p} \vert}{E}.
\end{equation}
If the quantum is physical, then $ E \geq m $ or, equivalently, $ \vert \mathbf{p} \vert < E $. Both of these conditions lead to a bound on the speed:
\begin{equation}
	0 \leq \vert \mathbf{v} \vert < 1.
\end{equation}
In contrast with energy, mass or spatial momentum, speed and velocity are dimensionless. However, energy and spatial momentum are conserved, but velocity is not. Given the speed, you can find the quantum's \textbf{rapidity} $\eta$ via the relation
\begin{equation}
	\eta = \operatorname{arctanh}{\vert \mathbf{v} \vert} = \frac{1}{2} \log{\left( \frac{1 + \vert \mathbf{v} \vert}{1 - \vert \mathbf{v} \vert} \right)}.
\end{equation}
Since the speed of a physical quantum is bounded, it follows that the rapidity of a physical quantum is bounded from below: $0 \leq \eta < \infty$.